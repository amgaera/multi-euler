import java.util {HashMap}


doc "Return a sorted list of factors of the given number.

	 The primes argument must be a sorted list of all primes less than
	 or equal to number / 2."
Integer[] factorise(Integer number, Integer[] primes) {
	variable Integer[] factors := {};
	variable value n := number;
	// There's no sense checking primes larger than half the original number
	variable value threshold := number / 2;

	for (prime in primes) {
		if (prime > threshold) {
			break;
		}

		while (n % prime == 0) {
			factors := append(factors, prime);
			n /= prime;
		}
	}

	// The number is prime
	if (factors.empty) {
		return {n};
	}

	return factors;
}


doc "Return the smallest positive number that is evenly divisible by all
	 of the numbers from 1 to divisorMax"
Integer findSmallestEvenlyDivisible(Integer divisorMax) {
	variable Integer[] primes := {};
	value factorCounter = HashMap<Integer, Integer>();

	/*
	 * This method works by factorising all numbers from 2 to divisorMax
	 * and recording the maximum number of times each prime figured on
	 * the list of factors of any one of them.
	 */
	for (divisor in 2..divisorMax) {
		value factors = factorise(divisor, primes);

		// The current divisor is prime
		if (factors.size == 1) {
			value factor = factors.first;

			if (exists factor) {
				primes := append(primes, factor);
				factorCounter.put(factor, 1);
			}
		}
		else {
			variable value currentFactor := factors.first else 0;
			variable value factorCount := 0;

			/*
			 * 0 is appended to the list of factors as a sentinel, so
			 * that we don't have to check the counter separately after
			 * the loop's finished.
			 */
			for (factor in append(factors, 0)) {
				if (factor == currentFactor) {
					++factorCount;
				}
				else {
					if (factorCount > factorCounter.get(currentFactor)) {
						factorCounter.put(currentFactor, factorCount);
					}

					currentFactor := factor;
					factorCount := 1;
			}
			}
		}
	}

	variable value result := 1;

	/*
	 * Finally, we multiply each prime as many times into the result as
	 * it's been spotted in a single divisor the most.
	 */
	for (prime in primes) {
		result *= prime ** factorCounter.get(prime);
	}

	return result;
}


void main() {
	print(findSmallestEvenlyDivisible(20));
}

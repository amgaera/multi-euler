import java.util {PriorityQueue, Comparator}


doc "Class storing multipliers along with their product."
class ProductTuple(x, y) {

	shared Integer x;
	shared Integer y;
	shared Integer product = x * y;

	shared actual String string = "ProductTuple(" + x.string + " * " +
		y.string + " = " + product.string + ")";
}


doc "Comparator used to put ProductTuples in decreasing product order."
class ProductTupleComparator() satisfies Comparator<ProductTuple> {

	shared actual Integer compare(ProductTuple? o1, ProductTuple? o2) {
		if (exists o1) {
			if (exists o2) {
				return o2.product - o1.product;
			}
		}

		return 0;
	}

	doc "Equals has to be refined because both Java's Comparator and
		 Ceylon's IdentifiableObject provide default implementations."
	shared actual Boolean equals(Object obj) {
		return false;
	}
}


doc "Check if the given word is a palindrome."
Boolean isPalindrome(String word) {
	return word.endsWith(word.initial(word.size / 2).reversed);
}


doc "Find the largest palindrome made from the product of two
	 multipliers with the given number of digits."
Integer? find_largest_palindromic_product(Integer mul_digits) {
	variable value mul := 10 ** mul_digits - 1;
	value mulMin = 10 ** (mul_digits - 1) - 1;
	variable value nextSeed := ProductTuple(mul, mul);
	// PriorityQueue is used to let us check products in decreasing order.
	value productQueue = PriorityQueue<ProductTuple>(11,
		ProductTupleComparator());

	/*
	 * The method used here could be considered semi-brute-force. We
	 * seed the productQueue with a ProductTuple storing the largest
	 * multiplier squared. Then we repeatedly pop the largest
	 * ProductTuple from the queue and check if the product it stores is
	 * palindromic. If it is, we've got our answer - we've been checking
	 * products in decreasing order, so the first match is the largest
	 * at the same time.
	 *
	 * Otherwise, we decrease the ProductTuple's second multiplier (the
	 * first one never changes once a ProductTuple's been added to the
	 * queue; this ensures that we don't have to deal with swapped
	 * multipliers) and, if it still has the required number of digits,
	 * add a new ProductTuple with the resulting product to the queue.
	 *
	 * After each iteration we also check if the next perfect square has
	 * the required number of digits and if it's larger than the queue's
	 * head. If this test succeeds, we add a corresponding ProductTuple
	 * to the queue (it will become the new head).
	 */
	productQueue.add(nextSeed);
	--mul;
	nextSeed := ProductTuple(mul, mul);

	while (!productQueue.size() == 0) {
		value item = productQueue.poll();

		if (isPalindrome(item.product.string)) {
			return item.product;
		}

		if (item.y - 1 > mulMin) {
			productQueue.add(ProductTuple(item.x, item.y - 1));
		}

		if (mul >= mulMin && productQueue.peek().product < nextSeed.product) {
			productQueue.add(nextSeed);
			--mul;
			nextSeed := ProductTuple(mul, mul);
		}
	}

	return null;
}


void main() {
	value result = find_largest_palindromic_product(3);

	if (exists result) {
		print(result);
	}
}

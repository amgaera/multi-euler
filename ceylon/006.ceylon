doc "Calculate the difference between the sum of the squares of the first n
     natural numbers and the square of the sum."
Integer diffSumSquaresSquareSum(Integer n) {
	variable Integer sum := 1;
	variable Integer result := 0;

	/*
	 * The method used here is based on the observation that:
	 * (1 + 2 + ... + n) ^ 2 - (1 ^ 2 + 2 ^ 2 + ... + n ^ 2) =
	 * = 2 * (2 * (1) + 3 * (2 + 1) + ... + n * (n_-1 + n_-2 + ... + 1))
	 */
	for (i in 2..n) {
		result += sum * i;
		sum += i;
	}

	return 2 * result;
}


void main() {
	print(diffSumSquaresSquareSum(100));
}

import java.util.List
import java.util.LinkedList


fun Double.isPerfectSquare() : Boolean {
	if (this < 0)
		return false

	return this.sqrt().toLong().toDouble().pow(2.0) == this
}


fun Double.sqrt() : Double {
	return Math.sqrt(this)
}


fun Double.floor() : Double {
	return Math.floor(this)
}


fun Double.pow(b : Double) : Double {
	return Math.pow(this, b)
}


/*
 * This poor man's max function is needed because Kotlin's generics
 * don't quite work yet (at least WRT the java.util.Collections stuff
 * that would normally be used for this).
 */
fun max(coll : List<Long>) : Long? {
	if (coll.isEmpty())
		return null

	var result : Long = coll[0]

	for (item in coll) {
		if (item > result)
			result = item
	}

	return result
}


fun gcd(a : Long, b : Long) : Long {
	return if (b == 0.toLong()) a else gcd(b, a % b)
}


inline fun squfofLoopIteration(kN : Double, p0 : Double, q0 : Double,
	q1 : Double) : #(Double, Double)
{
	val b = ((kN.sqrt().floor() + p0) / q1).floor()
	val p1 = b * q1 - p0
	val q2 = q0 + b * (p0 - p1)

	return #(p1, q2)
}


/*
 * Return a list of factors of N.
 *
 * This function calls findNonTrivialFactor repeatedly to compute all
 * factors of N.
 */
fun factorise(N : Long, retries : Byte) : List<Long> {
	var n = N
	val result = LinkedList<Long>()

	while (true) {
		val factor = findNonTrivialFactor(n, retries)

		if (factor == null) {
			result.add(n)
			break
		}
		else
			result.add(factor)
			n /= factor
	}

	return result
}


/*
 * Return a non-trivial factor of N, or null if N is prime.
 *
 * This function calls findFactor repeatedly with increasing values of
 * k in order to find a non-trivial factor of N. If after the specified
 * number of retries no non-trivial factors are found, null is returned
 * and N is considered prime.
 */
fun findNonTrivialFactor(N : Long, retries : Byte) : Long? {
	var k : Byte = 0

	do {
		k += 1
		val result = findFactor(N, k)

		if (result != 1.toLong() && result != N)
			return result
	} while (k <= retries)

	return null
}


/*
 * Return a (possibly trivial) factor of N.
 *
 * This function implements Shanks' square forms factorization
 * algorithm as described on Wikipedia. All searches should start with
 * k = 1, increasing this multiplier if the returned factor is trivial.
 */
fun findFactor(N : Long, k : Byte) : Long {
	val kN = (k * N).toDouble()

	var p0 = kN.sqrt().floor()
	var q0 = 1.0
	var q1 = kN - p0.pow(2.0)

	while (!q1.isPerfectSquare()) {
		val newVals = squfofLoopIteration(kN, p0, q0, q1)

		q0 = q1
		p0 = newVals._1
		q1 = newVals._2
	}

	q0 = q1.sqrt()
	p0 = ((kN.sqrt().floor() - p0) / q0).floor() * q0 + p0
	q1 = (kN - p0.pow(2.0)) / q0

	val orig_p0 = p0
	val orig_q0 = q0
	val orig_q1 = q1

	while (true) {
		val newVals = squfofLoopIteration(kN, p0, q0, q1)

		if (newVals._1 == p0)
			break

		q0 = q1
		p0 = newVals._1
		q1 = newVals._2

		/*
		 * This version of SQUFOF isn't supposed to be used with perfect
		 * squares and prime numbers and, as a result, the second loop
		 * seems to be infinite for certain primes. This comparison with
		 * the starting values lets us detect such cases and exit
		 * gracefully.
		 */
		if (p0 == orig_p0 && q0 == orig_q0 && q1 == orig_q1)
			return 1
	}

	return gcd(N, p0.toLong())
}


fun main(args : Array<String>) {
	println(max(factorise(600851475143, 5)))
}

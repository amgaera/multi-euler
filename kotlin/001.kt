fun main(args : Array<String>) {
	val multiples = 0..999 filter { it % 3 == 0 || it % 5 == 0 }
	val result = multiples.fold(0) { x, y -> x + y }
	println(result)
}
